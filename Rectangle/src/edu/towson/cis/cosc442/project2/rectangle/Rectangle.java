package edu.towson.cis.cosc442.project2.rectangle;

// TODO: Auto-generated Javadoc
/**
 * The Class Rectangle.
 */
public class Rectangle {
	
	/** The p2. */
	private Point p1, p2;
	
	/**
	 * Instantiates a new rectangle.
	 *
	 * @param p1 the p1
	 * @param p2 the p2
	 */
	Rectangle(Point p1, Point p2) {
		this.p1 = p1;
		this.p2 = p2;
	}
	
	/**
	 * Calculates the length of side A
	 * 
	 * @return x2 - x1
	 */
	private double xLength() {
		return p2.x - p1.x;
	}
	
	/**
	 * Calculates the length of side A
	 * 
	 * @return y2 - y1
	 */
	private double yLength() {
		return p2.y - p1.y;
	}

	/**
	 * Gets the area.
	 *
	 * @return the area
	 */
	public Double getArea() {
		return Math.abs(xLength() * yLength());
	}

	/**
	 * Gets the diagonal.
	 *
	 * @return the diagonal
	 */
	public Double getDiagonal() {
		return Math.sqrt(Math.pow(xLength(), 2) + Math.pow(yLength(), 2));
	}
}
